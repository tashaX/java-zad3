import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.json.*;
import java.net.*;

// Extend HttpServlet class to create Http Servlet
public class GetData extends HttpServlet {

  

   public void init() throws ServletException {
     
   }

   public void doGet(HttpServletRequest request, 
      HttpServletResponse response)
      throws ServletException, IOException 
   {
	  try {
		  String surl = "http://api.hnb.hr/tecajn/v1?datum=2018-10-24";
	  
		   URL obj = new URL(surl);
		   HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		   
		   int responseCode = con.getResponseCode();
		   System.out.println("\nSending 'GET' request to URL : " + surl);
		   System.out.println("Response Code : " + responseCode);
		   
		   BufferedReader in = new BufferedReader(
				   new InputStreamReader(con.getInputStream()));
		   String inputLine;
		   StringBuffer something = new StringBuffer();
		   while ((inputLine = in.readLine()) != null) {
			   something.append(inputLine);
		   }
		   in.close();
		   
		   System.out.println(something);
		  
		   JSONArray myResponse = new JSONArray(something.toString());
		   
		   JSONObject oneObj = myResponse.getJSONObject(0);
		   String brojTec = oneObj.getString("Broj tečajnice");
		   String dat = oneObj.getString("Datum primjene");   
		   System.out.println(myResponse.getClass());
		   System.out.println("array " + myResponse);
		   
		   response.setContentType("text/html");
		   response.setCharacterEncoding("UTF-8");
		   PrintWriter out = response.getWriter();
		   out.write("<!DOCTYPE html>");
		   out.write("<html>");
		   out.write("<head>");
		   out.write("<meta charset=\"utf-8\">");
		   out.write("<link rel=\"stylesheet\" href=\"grid.css\"/>");
		   out.write("</head>");
		   out.write("<body>");
		   out.write("<div class='container'>");
		   out.write("<div class='row'><div class='col-s-6'><h2>Broj tečajnice: " + brojTec + 
				   	"</h2></div><div class='col-s-6'><h2>Datum primjene: " + dat + "</h2></div></div>");
		   
		   for(int i=0; i<myResponse.length(); i++) {
			   JSONObject objT =  myResponse.getJSONObject(i);
			   System.out.println("obj " + objT.toString());
			   
			   String drzava = objT.getString("Država");
			   String sifVal = objT.getString("Šifra valute");
			   String val = objT.getString("Valuta");
			   Integer jed = objT.getInt("Jedinica");
			   String kup = objT.getString("Kupovni za devize");
			   String sred = objT.getString("Srednji za devize");
			   String prod = objT.getString("Prodajni za devize");
			   
			  
			   out.write("<div class='row'>");
			  
			   out.write("<div class='col-m-2'>Drzava: " + drzava + "</div>");
			   out.write("<div class='col-m-2'>Šifra valute: " + sifVal + "</div>");
			   out.write("<div class='col-m-2'>Jedinica: " + jed + "</div>");
			   out.write("<div class='col-m-2'>Kupovni za devize: " + kup + "</div>");
			   out.write("<div class='col-m-2'>Srednji za devize: " + sred + "</div>");
			   out.write("<div class='col-m-2'>Prodajni za devize: " + prod + "</div>");
			   out.write("</div>");


		   } 
		   
		   
		   out.write("</div></body></html>");
		   
		   
		   out.close();
		   
	  } catch(Exception e) {
		  System.out.println(e);
	  }
	   
	   
	    
   }

   public void destroy() {
      /* leaving empty for now this can be
       * used when we want to do something at the end
       * of Servlet life cycle
       */
   }
}